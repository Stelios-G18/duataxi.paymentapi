﻿using DuaTaxi.Service.Payments.Entities.Models;
using DuaTaxi.Service.Payments.Repository;
using DuaTaxi.Service.Payments.Services.DayOfExpiration;
using DuaTaxi.Service.Payments.Services.FindExistingPayments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Service.Payments.Services.CreateNewPayment
{
    public class CreateNewPayment : ICreateNewPayment
    {
        private readonly IFindExistingPayments _findExistingPayments;
        private readonly IDayOfExpirationService _dayOfExpirationService;
        private readonly IPaymentRepository _paymentRepository;
        public CreateNewPayment(
            IFindExistingPayments findExistingPayments,
            IDayOfExpirationService dayOfExpirationService,
            IPaymentRepository paymentRepository)
        {
            _findExistingPayments = findExistingPayments;
            _dayOfExpirationService = dayOfExpirationService;
            _paymentRepository = paymentRepository;
        }
       

        public async Task<Payment> NewPayment(Payment payment)
        {
            double DaysUntilExpires;
            var activepayment = await _findExistingPayments.FindActivePayment(payment.CustomerId);
            
            if (activepayment == null) {
                // create new payment
                // i have to create after free period expired payment with zero days 
                AddNewPayment(payment);
            }

            activepayment.DayOfExpiration = await _dayOfExpirationService.GetLeftDaysOfExpirationById(activepayment.Id);         

            if (activepayment.DayOfExpiration > 0.00)
                DaysUntilExpires = activepayment.DayOfExpiration;
            else
                DaysUntilExpires = 0.00;  

            
            UpdateActivePayment(activepayment);
            
            AddNewPayment(payment,DaysUntilExpires);

            return null;
        }

        public async void AddNewPayment(Payment payment, double daysUntilExpires = 0.00)
        {
            double findDays = _dayOfExpirationService.SetDayOfExpiration(payment.PaymentType) + daysUntilExpires;

            var newPayment = new Payment(payment.Id, payment.CustomerId, payment.Name, payment.Email, payment.PhoneNumber, payment.Type, payment.PaymentType, true, findDays, payment.Amount);
            await _paymentRepository.AddAsync(newPayment);

        }

        public void UpdateActivePayment(Payment activepayment) {
            activepayment.Active = false;                                  
            activepayment.UpdatedDate = DateTime.Now;
            _paymentRepository.UpdateAsync(activepayment);

        }
    }
}
