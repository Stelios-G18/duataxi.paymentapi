﻿using DuaTaxi.Service.Payments.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Service.Payments.Services.CreateNewPayment
{
    public interface ICreateNewPayment
    {
        Task<Payment> NewPayment(Payment payment);
    }
}
