#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine  AS base
WORKDIR /app
ENV ASPNETCORE_URLS http://*:5002
ENV ASPNETCORE_ENVIRONMENT docker
EXPOSE 5002

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /src
COPY ["DuaTaxi.Service.Payments/DuaTaxi.Service.Payments.csproj", "DuaTaxi.Service.Payments/"]
RUN dotnet restore "DuaTaxi.Service.Payments/DuaTaxi.Service.Payments.csproj"
COPY . .
WORKDIR "/src/DuaTaxi.Service.Payments"
RUN dotnet build "DuaTaxi.Service.Payments.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DuaTaxi.Service.Payments.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DuaTaxi.Service.Payments.dll"]