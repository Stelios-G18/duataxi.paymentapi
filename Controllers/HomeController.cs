﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Service.Payments.Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.Service.Payments.Controllers
{
    [Route("")]
    [ServiceFilter(typeof(AuthValidation))]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get() => Ok("DuaTaxi Payment Service");

       
    }
}