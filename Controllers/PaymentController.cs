﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Service.Payments.Entities.Models;
using DuaTaxi.Service.Payments.Services.CreateNewPayment;
using DuaTaxi.Service.Payments.Services.DayOfExpiration;
using DuaTaxi.Service.Payments.Services.FindExistingPayments;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.Service.Payments.Controllers
{
    [Route("api/[controller]")]
    [ServiceFilter(typeof(AuthValidation))]
    public class PaymentController : ControllerBase
    {
        private readonly IFindExistingPayments _findExistingPayments;
        private readonly IDayOfExpirationService _dayOfExpirationService;
        private readonly ICreateNewPayment _newPayment;
        public PaymentController(IFindExistingPayments findExistingPayments, IDayOfExpirationService dayOfExpirationService, ICreateNewPayment newPayment)
        {
            _findExistingPayments = findExistingPayments;
            _dayOfExpirationService = dayOfExpirationService;
            _newPayment = newPayment;
        }


        [HttpGet]
        [Route("GetByCustomerId/{customerId}")]
        public async Task<SrvResp<Payment>> GetByCustomerId(string customerId)
        {
            var activepayment = await _findExistingPayments.FindActivePayment(customerId);
            if (activepayment == null) {
                return new SrvResp<Payment>();
            }
            activepayment.DayOfExpiration = await _dayOfExpirationService.GetLeftDaysOfExpirationById(activepayment.Id);
            return new SrvResp<Payment>(activepayment);
        }


        [HttpGet]
        [Route("Notification/")]
        public async Task<SrvResp<IEnumerable<Payment>>> Notification()
        {
            var activepaymentWhereExpiredIn3days = await _findExistingPayments.FindActivePaymentToBeExpired();
            if (activepaymentWhereExpiredIn3days == null) {
                return new SrvResp<IEnumerable<Payment>>();
            }
            return new SrvResp<IEnumerable<Payment>>(activepaymentWhereExpiredIn3days);
        }


        [HttpPost]
        [Route("Create/")]
        public async Task<SrvResp<Payment>> Create([FromBody] Payment payment)
        {

            var newPayment = await _newPayment.NewPayment(payment);

            return new SrvResp<Payment>();

        }
    }
}